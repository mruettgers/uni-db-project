﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using DBForum.Models;
using DBForum.Models.PraktikumTableAdapters;

namespace DBForum.Controllers
{
    public class BenutzerController : Controller
    {

        // GET: /Benutzer/
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        // GET: /Benutzer/List
        public ActionResult List()
        {
            return View(Benutzer.GetBenutzer());
        }



        // GET: /Benutzer/Statistik
        public ActionResult Stats(string c)
        {
            if (string.IsNullOrEmpty(c))
                c = "";
            ViewBag.ReqChar = c.ToLower();

            var adapter = new BenutzerrollenTableAdapter();

            Praktikum.BenutzerrollenDataTable result;
            if (string.IsNullOrEmpty(c))
                result = adapter.GetData();
            else
                result = adapter.GetDataByFirstChar(c);

            return View(result);
        }


        // GET: /Benutzer/Statistik
        public ActionResult StatsChart(string c)
        {
            if (string.IsNullOrEmpty(c))
                c = "";
            ViewBag.ReqChar = c.ToLower();

            var adapter = new BenutzerrollenTableAdapter();
            Praktikum.BenutzerrollenDataTable result;
            if (string.IsNullOrEmpty(c))
                result = adapter.GetData();
            else
                result = adapter.GetDataByFirstChar(c);

            var amounts = new Dictionary<string, int>();
            foreach (var user in result)
            {
                if (!amounts.ContainsKey(user.Rolle))
                    amounts[user.Rolle] = 1;
                else
                    amounts[user.Rolle]++;
            }

            var chart = new Chart(width: 600, height: 400).AddSeries(
                chartType: "pie",
                legend: "Verteilung der Benutzerrollen",
                xValue: amounts.Keys,
                yValues: amounts.Values).Write("png");
            return null;
        }


        // GET: /Benutzer/Summary
        public ActionResult Summary(string user)
        {
            return View(Benutzer.GetBenutzerByNickname(user));
        }

        // GET: /Benutzer/Add
        public ActionResult Add()
        {
            return View(new Student());
        }

        // POST: /Benutzer/Add
        [HttpPost]
        public ActionResult Add(FormCollection fc)
        {
           
            var student = new Student();
            student.Nickname = fc["Nickname"];
            student.Vorname = fc["Vorname"];
            student.Nachname = fc["Nachname"];
            student.Email = fc["Email"];
            student.Passwort = fc["Passwort"];

            int tmp = 0;
            Int32.TryParse(fc["MatrikelNummer"], out tmp);
            student.MatrikelNummer = tmp;

            DateTime tmpdate;
            DateTime.TryParse(fc["EinschreibeDatum"], out tmpdate);
            student.EinschreibeDatum = tmpdate;

            ViewBag.ErrorMessages = null;
            var errorMessages = new List<string>();
            var valid = student.Validate(ref errorMessages);
            if (fc["Passwort"].Length == 0 || fc["Passwort"] != fc["PasswortWdh"])
            {
                valid = false;
                errorMessages.Add("Das Passwort ist ungültig oder es stimmt nicht mit der Wiederholung überein.");
            }

            if (valid)
            {
                student.Save();
                FlashMessage.AddFlashMessage( "Der Benutzer wurde erfolgreich hinzugefügt.", FlashMessage.MessageType.Success);
                return RedirectToAction("List");
            }
            else
                ViewBag.ErrorMessages = errorMessages;

            return View(student);
        }




    }
}
