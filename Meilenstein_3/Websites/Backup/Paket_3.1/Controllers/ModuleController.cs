﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBForum.Models;

namespace DBForum.Controllers
{
    public class ModuleController : Controller
    {
        
        // GET: /Module/
        public ActionResult Index()
        {
            BreadCrumb.ClearBreadCrumbs();
            return RedirectToAction("List");
        }

        // GET: /Module/List
        public ActionResult List()
        {
            return View(Modul.GetModule());
        }

    }
}
