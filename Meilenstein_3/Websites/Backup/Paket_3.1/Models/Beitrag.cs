﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace DBForum.Models
{
    public class Beitrag
    {

        public int ID { set; get; }
        public string Mitteilung { set; get; }
        public DateTime AenderungsDatum { set; get; }

        public static List<Beitrag> GetBeitraegeByForumAndUser(int forum, string nickname)
        {
            return GetBeitraegeByForumAndUser(forum, nickname, "Änderungsdatum DESC");
        }

        public static List<Beitrag> GetBeitraegeByForumAndUser(int forum , string nickname, string sorting)
        {
            var result = new List<Beitrag>();

            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
            string query = "SELECT b.ID, b.Mitteilung, [Änderungsdatum] AS AendDatum " +
                           " FROM [Beiträge] AS b " +
                           " INNER JOIN Diskussionen AS d ON d.ID=b.DiskussionsID" +
                           " WHERE " +
                           " d.ForumID=@ForumID AND b.Benutzer = @Nickname";

            if (sorting != null)
                query += " ORDER BY " + sorting;
            
            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Nickname", nickname);
            cmd.Parameters.AddWithValue("@ForumID", forum);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    var beitrag = new Beitrag();
                    beitrag.ID = (int)reader["ID"];
                    if (!String.IsNullOrWhiteSpace(reader["AendDatum"].ToString()))
                    {
                        beitrag.AenderungsDatum = (DateTime) reader["AendDatum"];
                    }
                    beitrag.Mitteilung = reader["Mitteilung"].ToString();
                    result.Add(beitrag);
                }
            }
            conn.Close();

            return result;
        }  

    }
}