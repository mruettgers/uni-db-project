﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace DBForum.Models
{
    public class BreadCrumb
    {

        public string Action { get; set; }
        public string Controller { get; set; }
        public string Module { get; set; }
        public object Parameters { get; set; }
        public string Title { get; set; }
        public Boolean Active { get; set; }

        public BreadCrumb()
        {
            Module = "";
            Active = false;
            Controller = "";
            Title = "";
            Active = false;
            Parameters = new object();
        }

        public Boolean IsActive()
        {
            return Active;
        }        

        private static List<BreadCrumb> _breadcrumbs = null;

        public static List<BreadCrumb> GetBreadCrumbs()
        {
            if (_breadcrumbs == null)
            {
                _breadcrumbs = new List<BreadCrumb>();
            }
            return _breadcrumbs;
        }

        public static void ClearBreadCrumbs()
        {
            GetBreadCrumbs().Clear();
        }

        public static void AddBreadCrumb(BreadCrumb bc)
        {
            GetBreadCrumbs().Add(bc);
        }

    }
}