﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Helpers;
using System.Web.Mvc;

namespace DBForum.Models
{
    public class Benutzer
    {

        protected enum SaveActionType
        {
            Create,
            Update
        };

        public string Nickname { get; set; }
        public string Vorname { get; set; }
        public string Nachname { get; set; }

        protected SaveActionType SaveAction { get; set; }


        private string _password = "";
        public string Passwort
        {
            get
            {
                return _password;
            }
            set
            {
                _password = Crypto.Hash(value, "md5");
            }
        }

        public string Email { get; set; }
        public string Typ { get; set; }

        private List<ForumBeitrag> _forenbeitraege;

        public List<ForumBeitrag> ForenBeitraege
        {
            get
            {
                if (_forenbeitraege == null)
                {
                    _forenbeitraege = ForumBeitrag.GetForenBeitraegeByUser(Nickname);
                }
                return _forenbeitraege;
            }
            set
            {
                _forenbeitraege = value;
            }
        }


        private List<Dokument> _dokumente;
        public List<Dokument> Dokumente
        {
            get
            {
                if (_dokumente == null) 
                    _dokumente = Dokument.GetDokumenteByUser(Nickname);
                return _dokumente;
            }
            set
            {
                _dokumente = value;
            }
        }


        public Benutzer()
        {
            _dokumente = null;
            SaveAction = SaveActionType.Create;
        }

        public void Load(Benutzer user)
        {
            _dokumente = null;
            if (user == null)
                throw new Exception("Der übergebene Benutzer ist ungültig.");
            Nickname = user.Nickname;
            Vorname = user.Vorname;
            Nachname = user.Nachname;
            Email = user.Email;
            Passwort = user.Passwort;
            Typ = user.Typ;
        }


        public static Boolean ExistsBenutzer(string nickname, string email)
        {
            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
            string query = " SELECT COUNT(*) AS AnzBenutzer " +
                                 " FROM [Benutzer]" +
                                 " WHERE 1=1 ";

            if (nickname != null)
                query += " AND [Benutzer].[Nickname] = @Nickname";

            if (email != null)
                query += " AND [Benutzer].[Email] LIKE @Email";

            var cmd = new SqlCommand(query, conn);

            if (nickname != null) 
                cmd.Parameters.AddWithValue("@Nickname", nickname);
            if (email != null)
                cmd.Parameters.AddWithValue("@Email", email);
            

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    int tmp;
                    Int32.TryParse(reader["AnzBenutzer"].ToString(), out tmp);
                    if (tmp == 0)
                        return false;
                }
            }
            conn.Close();
            return true;
        }




        //Return one user with given Nickname
        public static Benutzer GetBenutzerByNickname(string nickname)
        {
            Benutzer result = null;
            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
            const string query = " SELECT Benutzer.*, 'Professor' AS Typ " +
                           " FROM [Benutzer], [Professoren]" +
                           " WHERE [Professoren].[Nickname] = [Benutzer].[Nickname]" +
                           "   AND [Benutzer].[Nickname] = @Nickname"+
                           " UNION" +
                           " SELECT Benutzer.*, 'Student' AS Typ " +
                           " FROM [Benutzer], [Studenten]" +
                           " WHERE [Studenten].[Nickname] = [Benutzer].[Nickname]" +
                           "   AND [Benutzer].[Nickname] = @Nickname" +
                           " UNION" +
                           " SELECT Benutzer.*, 'Mitarbeiter' AS Typ " +
                           " FROM [Benutzer], [Mitarbeiter]" +
                           " WHERE [Mitarbeiter].[Nickname] = [Benutzer].[Nickname]"+
                           "   AND [Benutzer].[Nickname] = @Nickname";
            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Nickname", nickname);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if  (reader.Read())
                {
                    result = new Benutzer();
                    result.SaveAction = SaveActionType.Update;
                    result.Nickname = reader["Nickname"].ToString();
                    result.Vorname = reader["Vorname"].ToString();
                    result.Nachname = reader["Nachname"].ToString();
                    result.Email = reader["Email"].ToString();
                    result.Passwort = reader["Passwort"].ToString();
                    result.Typ = reader["Typ"].ToString();
                    
                }
            }
            conn.Close();
            return result;
           
        }


        //Returns a list of users
        public static List<Benutzer> GetBenutzer()
        {
            return GetBenutzer("Nickname");
        }

        public static List<Benutzer> GetBenutzer(string sorting)
        {
            var result = new List<Benutzer>();
            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
            string query = " SELECT Benutzer.*, 'Professor' AS Typ " +
                           " FROM [Benutzer], [Professoren]" +
                           " WHERE [Professoren].[Nickname] = [Benutzer].[Nickname]" +
                           " UNION" +
                           " SELECT Benutzer.*, 'Student' AS Typ " +
                           " FROM [Benutzer], [Studenten]" +
                           " WHERE [Studenten].[Nickname] = [Benutzer].[Nickname]" +
                           " UNION" +
                           " SELECT Benutzer.*, 'Mitarbeiter' AS Typ " +
                           " FROM [Benutzer], [Mitarbeiter]" +
                           " WHERE [Mitarbeiter].[Nickname] = [Benutzer].[Nickname]" +
                           " ORDER BY " + sorting;

            var cmd = new SqlCommand(query, conn);
            using(SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read()) {
                    var user = new Benutzer();
                    user.SaveAction = SaveActionType.Update;
                    user.Nickname = reader["Nickname"].ToString();
                    user.Vorname = reader["Vorname"].ToString();
                    user.Nachname = reader["Nachname"].ToString();
                    user.Email = reader["Email"].ToString();
                    user.Passwort = reader["Passwort"].ToString();
                    user.Typ = reader["Typ"].ToString();
                    result.Add(user);
                }
            }
            conn.Close();
            return result;
        }


        public Mitarbeiter AsMitarbeiter()
        {
            Typ = "Mitarbeiter";
            return new Mitarbeiter(this);  
        }

        public Student AsStudent()
        {
            Typ = "Student";
            return new Student(this);
            
        }

        public Professor AsProfessor()
        {
            Typ = "Professor";
            return new Professor(this);
            
        }

        public virtual bool Validate(ref List<String> errorMessages)
        {
            if (errorMessages == null)
            {
                errorMessages = new List<string>();
            }

            var res = true; 
            
            if (Nickname.Length == 0)
            {
                errorMessages.Add("Es wurde kein Nickname angegeben.");
                res = false;
            }
            else if (ExistsBenutzer(Nickname,null)) 
            {
                errorMessages.Add("Es existiert bereits ein Benutzer mit diesem Nickname.");
                res = false;
            }
            else if (Email.Length == 0)
            {
                errorMessages.Add("Es wurde keine E-Mail-Adresse angegeben.");
                res = false;
            }
            else if (ExistsBenutzer(null,Email))
            {
                errorMessages.Add("Es existiert bereits ein Benutzer mit dieser E-Mail-Adresse.");
                res = false;
            }
            return res;
        }

        public virtual void Save()
        {
            //Makes the current user persistent
            string query = "";
            if (SaveAction == SaveActionType.Update)
            {
                throw new Exception("Update action is not implemented yet.");
                /*
                query = "UPDATE Benutzer SET ";
                query += " WHERE Nickname=@Nickname";
                */
                
            }
            else if (SaveAction == SaveActionType.Create)
            {
                query = "INSERT INTO Benutzer (Nickname,Nachname,Vorname,Email,Passwort) VALUES " +
                        " (@Nickname,@Nachname,@Vorname,@Email,@Passwort)";

            }

            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
          
            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Nickname", Nickname);
            cmd.Parameters.AddWithValue("@Nachname", Nachname);
            cmd.Parameters.AddWithValue("@Vorname", Vorname);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Passwort", Passwort);
            cmd.ExecuteNonQuery();
            conn.Close();

        }

    }


}