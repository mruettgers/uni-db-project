﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace DBForum.Models
{
    public class Modul
    {

        public string FachNummer { get; set; }
        public string Bezeichnung { get; set; }
        public string Verantwortlicher { get; set; }
        public string VerantwortlicherName { get; set; }

        public static List<Modul> GetModuleByUser(string nickname)
        {
            return GetModule("Bezeichnung", nickname);
        } 

        public static List<Modul> GetModule()
        {
            return GetModule("Bezeichnung");
        }

        public static List<Modul> GetModule(string sorting)
        {
            return GetModule(sorting, null);
        }
        
        public static List<Modul> GetModule(string sorting, string nickname)
        {
            var result = new List<Modul>();
            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
            string query =
                "SELECT Module.Fachnummer, Module.Bezeichnung, Professoren.AkademischerTitel, Benutzer.Nickname, Benutzer.Nachname, Benutzer.Vorname" +
                " FROM Module, Professoren, Benutzer" +
                " WHERE Professoren.Nickname=Module.Verantwortlicher AND Benutzer.Nickname=Module.Verantwortlicher" +
                ((nickname != null) ? " AND Professoren.Nickname=@Nickname" : "");
            
            if (sorting != null)
            {
                query += " ORDER BY " + sorting;
            }

            var cmd = new SqlCommand(query, conn);
            if (nickname != null)
                cmd.Parameters.AddWithValue("@Nickname", nickname);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    var module = new Modul();
                    module.FachNummer = reader["FachNummer"].ToString();
                    module.Bezeichnung = reader["Bezeichnung"].ToString();
                    module.Verantwortlicher = reader["Nickname"].ToString();
                    module.VerantwortlicherName = reader["AkademischerTitel"] + " " + reader["Vorname"].ToString() + " " +
                                              reader["Nachname"].ToString();
                    
                    result.Add(module);
                }
            }
            conn.Close();
            return result;
        }

    }
}