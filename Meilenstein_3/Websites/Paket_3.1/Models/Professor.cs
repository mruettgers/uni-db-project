﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Data.SqlClient;

namespace DBForum.Models
{
    public class Professor : Benutzer
    {
      
        public string AkademischerTitel { get; set; }
      
        private List<Modul> _module;
        public List<Modul> Module
        {
            get
            {
                if (_module == null) 
                    _module = Modul.GetModuleByUser(Nickname);
                return _module;
            }
            set
            {
                _module = value;
            }
        }


        public Professor()
        {
            _module = null;
            Typ = "Professor";
        }


        public Professor(Benutzer user)
        {
            Load(user);
            
            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
            string query = " SELECT Professoren.*" +
                           " FROM [Professoren]" +
                           " WHERE [Professoren].[Nickname] = @Nickname";

            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Nickname", Nickname);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    AkademischerTitel = reader["AkademischerTitel"].ToString();
                }
                else throw new Exception("Der Professor existiert nicht.");
            }
            conn.Close();
        }

        public static Professor GetProfessorByUser(string nickname)
        {
            return new Professor(Benutzer.GetBenutzerByNickname(nickname));
        }
    }
}