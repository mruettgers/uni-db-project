﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;

namespace DBForum.Models.Linq
{
    public class UnterForenUndDiskussionen
    {
        public Foren AktuellesForum { get; set; }
        public Stack<Foren> Path { get; set; } 
        public IEnumerable<Foren> Foren { get; set; } 
        public IEnumerable<Diskussionen> Diskussionen { get; set; }

        public UnterForenUndDiskussionen(int forumId = 0)
        {
            var dc = new DataClasses1DataContext();
            AktuellesForum = null;
            Foren = null;
            Diskussionen = null;
            Path = null;
            if (forumId != 0)
            {
                var fp = new ForenPfad(forumId);

                if (fp.AktuellesForum != null)
                {

                    AktuellesForum = fp.AktuellesForum;
                    Path = fp.Path;

                    //Load sub forums and discussions
                    Foren = from forum in dc.Foren
                        where forum.OberforumID == AktuellesForum.ID
                        orderby forum.Bezeichnung
                        select forum;
                    Diskussionen = from diskussion in dc.Diskussionen
                        where diskussion.ForumID == AktuellesForum.ID
                        orderby diskussion.Titel
                        select diskussion;

                }
            }
            else
            {

                Foren = from forum in dc.Foren
                    where forum.OberforumID == null
                    orderby forum.Bezeichnung
                    select forum;

            }

        }
    }
}