﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBForum.Models.Linq;
using DBForum.Models;

namespace DBForum.Controllers
{
    public class ForenController : Controller
    {
        //
        // GET: /Foren/

        public ActionResult Index(int id = 0)
        {
            var model = new UnterForenUndDiskussionen(id);
            return View(model);
        }
        
        // GET: /Foren/AddDiscussion
        [HttpPost]
        public ActionResult AddDiscussion(int id, NeueDiskussion d)
        {
            var errorMessages = new List<string>();
            d.SetForumId(id);

            if (d.Validate(ref errorMessages))
            {
                d.Save();
            }          

            if (errorMessages.Count == 0)
            {
                FlashMessage.AddFlashMessage("Die Diskussion wurde erfolgreich erstellt.", FlashMessage.MessageType.Success);
                return RedirectToAction("Index", new {id = d.AktuellesForum.ID});
            }
            else
            {
                ViewBag.ErrorMessages = errorMessages;
            }
            return View(d);
        }

        public ActionResult AddDiscussion(int id)
        {
            return View(new NeueDiskussion(id));
        }
    }
}
