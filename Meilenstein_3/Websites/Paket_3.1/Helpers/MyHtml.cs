﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace DBForum.Helpers
{
    public static class MyHtml
    {
        static Regex LineEnding = new Regex(@"(\r\n|\r|\n)+");

        public static MvcHtmlString Nl2Br(this HtmlHelper html, string text, bool isXhtml = true)
        {
            var encodedText = HttpUtility.HtmlEncode(text);
            var replacement = isXhtml ? "<br />" : "<br>";
            return MvcHtmlString.Create(LineEnding.Replace(encodedText, replacement));
        }
    }
}