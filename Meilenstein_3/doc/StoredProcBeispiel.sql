-- definition
CREATE PROCEDURE DiskussionGesichtet
	@ID int
AS
	-- increment AnzahlSichtungen by 1
    UPDATE Diskussionen SET AnzahlSichtungen = AnzahlSichtungen + 1
    WHERE ID = @ID
GO

-- example
EXECUTE DiskussionGesichtet 5;

GO

-- test for success
SELECT * FROM Diskussionen WHERE ID = 5


