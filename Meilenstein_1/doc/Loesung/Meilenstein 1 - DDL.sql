
------ Batch f�rs Praktikum
--- Die eckigen Klammern - z.b. [Professoren] stellen sicher, dass Sonderzeichen
--- Leerzeichen oder sogar Schl�sselworte als Tabellen/Spaltennamen genutzt werden k�nnen.
--- So k�nnte man eine Tabelle auch [Table] oder [Stra�e und Hausnummer] nennen.

-- Nutze Praktikum Datenbank



USE [Praktikum]

GO

-- Bedingtes L�schen: Sofern in den Systemtabellen die entsprechenden Tabellen 
-- verzeichnet sind, werden diese gedropped.

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BeitragHasDokument]') AND type in (N'U'))
DROP TABLE [BeitragHasDokument]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dokumente]') AND type in (N'U'))
DROP TABLE [Dokumente]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Module]') AND type in (N'U'))
DROP TABLE [Module]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Beitr�ge]') AND type in (N'U'))
DROP TABLE [Beitr�ge]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Diskussionen]') AND type in (N'U'))
DROP TABLE [Diskussionen]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Professoren]') AND type in (N'U'))
DROP TABLE [Professoren]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Mitarbeiter]') AND type in (N'U'))
DROP TABLE [Mitarbeiter]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Studenten]') AND type in (N'U'))
DROP TABLE [Studenten]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Benutzer]') AND type in (N'U'))
DROP TABLE [Benutzer]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Foren]') AND type in (N'U'))
DROP TABLE [Foren]

GO

-- Entities


CREATE TABLE [Foren](
	[ID] [bigint] IDENTITY(1,1) PRIMARY KEY, 	
	[Bezeichnung] [varchar](256) NOT NULL,
	[OberforumID] [bigint] NULL 
		REFERENCES [Foren]
)

GO

CREATE TABLE [Diskussionen](
	[ID] [bigint] IDENTITY(1,1) PRIMARY KEY, 	
	[Titel] [varchar](256) NOT NULL,
	[AnzahlSichtungen] [int] NOT NULL DEFAULT 0,
	[ForumID] [bigint] NOT NULL
		REFERENCES [Foren]	
)

GO

CREATE TABLE [Benutzer](
	[Nickname] [varchar](32) PRIMARY KEY, 
	[Vorname] [varchar](32) NULL,
	[Nachname] [varchar](32) NULL,
	[Passwort] [char](32) NOT NULL,
	[Email] [varchar](256) NULL UNIQUE
)

GO

CREATE TABLE [Beitr�ge](
	[ID] [bigint] IDENTITY(1,1) PRIMARY KEY, 
	[Mitteilung] [varchar](max) NOT NULL,
	[�nderungsdatum] [datetime] NULL DEFAULT CURRENT_TIMESTAMP,
	[DiskussionID] [bigint] NOT NULL
		REFERENCES [Diskussionen],
	[BenutzerID] [varchar](32) NULL
		REFERENCES [Benutzer],
	[Ver�ffentlichungsdatum] [datetime] NOT NULL,
			
)

GO

CREATE TABLE [Studenten](
	[ID] [varchar](32) PRIMARY KEY 
		REFERENCES [Benutzer] ON DELETE CASCADE, 
	[MatrikelNr] [int] NOT NULL UNIQUE,
	[Einschreibedatum] [datetime]
)

GO

CREATE TABLE [Professoren](
	[ID][varchar](32) PRIMARY KEY 
		REFERENCES [Benutzer] ON DELETE CASCADE, 
	[AkademischerTitel] [varchar](64) NOT NULL
)

GO

CREATE TABLE [Mitarbeiter](
	[ID] [varchar](32) PRIMARY KEY 
		REFERENCES [Benutzer]  ON DELETE CASCADE,
	[RaumNr] [varchar](16) NOT NULL,
	[Aufgabenbereich] [varchar](256) NOT NULL
)

GO

CREATE TABLE [Module](
	[FachNummer] [bigint] PRIMARY KEY CHECK ([FachNummer] BETWEEN 500 AND 999), 
	[Bezeichnung] [varchar](64) NOT NULL,
	[ForumID] [bigint] NULL 
		REFERENCES [Foren],
	[ProfessorID] [varchar](32) NOT NULL
		REFERENCES [Professoren]
)

GO

CREATE TABLE [Dokumente](
	[ID] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[Kategorie] [varchar](64) CHECK ([Kategorie] IN ('Vorlesung','�bung','Praktikum','Sonstiges')),
	[Datei] [varbinary](max),
	[Titel] [varchar](256),
	[Bereitstellungsdatum] [datetime],
	[ModulID] [bigint] NOT NULL
		REFERENCES [Module],
	[BenutzerID] [varchar](32) NOT NULL
		REFERENCES [Benutzer]
)

GO

-- Relationen

CREATE TABLE [BeitragHasDokument] (
	[DokumentID] [bigint] NOT NULL 
		REFERENCES [Dokumente],
	[BeitragID] [bigint] NOT NULL 
		REFERENCES [Beitr�ge],
	CONSTRAINT PK_BeitragHasDokument PRIMARY KEY (DokumentID, BeitragID)
)

GO

-- Trigger

CREATE TRIGGER TR_STUDENT_DELETE
    ON [Studenten]
    FOR DELETE
AS
    DELETE FROM [Benutzer]
    WHERE [Nickname] IN(SELECT deleted.id FROM deleted)

GO

CREATE TRIGGER TR_PROFESSOR_DELETE
    ON [Professoren]
    FOR DELETE
AS
    DELETE FROM [Benutzer]
    WHERE [Nickname] IN(SELECT deleted.id FROM deleted)

GO

CREATE TRIGGER TR_MITARBEITER_DELETE
    ON [Mitarbeiter]
    FOR DELETE
AS
    DELETE FROM [Benutzer]
    WHERE [Nickname] IN(SELECT deleted.id FROM deleted)
