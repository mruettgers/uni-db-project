
------ Batch f�rs Praktikum
--- Die eckigen Klammern - z.b. [Professoren] stellen sicher, dass Sonderzeichen
--- Leerzeichen oder sogar Schl�sselworte als Tabellen/Spaltennamen genutzt werden k�nnen.
--- So k�nnte man eine Tabelle auch [Table] oder [Stra�e und Hausnummer] nennen.

-- Nutze Praktikum Datenbank

USE [Praktikum]

GO

-- Bedingtes L�schen: Sofern in den Systemtabellen die entsprechenden Tabellen 
-- verzeichnet sind, werden diese gedropped.

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dokumente]') AND type in (N'U'))
DROP TABLE [Dokumente]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Module]') AND type in (N'U'))
DROP TABLE [Module]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Diskussionen]') AND type in (N'U'))
DROP TABLE [Diskussionen]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Professoren]') AND type in (N'U'))
DROP TABLE [Professoren]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Mitarbeiter]') AND type in (N'U'))
DROP TABLE [Mitarbeiter]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Beitr�ge]') AND type in (N'U'))
DROP TABLE [Beitr�ge]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Studenten]') AND type in (N'U'))
DROP TABLE [Studenten]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Benutzer]') AND type in (N'U'))
DROP TABLE [Benutzer]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Foren]') AND type in (N'U'))
DROP TABLE [Foren]

GO

-- Entities


CREATE TABLE [Foren](
	[Bezeichnung] [varchar](50),
	[OberforumID] [varchar](50)
)

GO

CREATE TABLE [Diskussionen](
	[Titel] [varchar](50),
	[AnzahlSichtungen] [varchar](50),
	[ForumID] [varchar](50)
)

GO

CREATE TABLE [Beitr�ge](
	[Mitteilung] [varchar](50),
	[�nderungsdatum] [varchar](50)
)

GO

CREATE TABLE [Benutzer](
	[Nickname] [varchar](50),
	[Vorname] [varchar](50),
	[Nachname] [varchar](50),
	[Passwort] [varchar](50),
	[Email] [varchar](50)
)

GO

CREATE TABLE [Studenten](
	[Matrikel] [varchar](50),
	[EinschreibeDatum] [varchar](50)
)

CREATE TABLE [Professoren](
	[AkademischerTitel] [varchar](50)
)

CREATE TABLE [Mitarbeiter](
	[RaumNr] [varchar](50),
	[Aufgabenbereich] [varchar](50)
)

GO

CREATE TABLE [Module](
	[Bezeichnung] [varchar](50),
	[FachNummer] [varchar](50)
)

GO

CREATE TABLE [Dokumente](
	[Kategorie] [varchar](50),
	[Datei] [varchar](50),
	[Titel] [varchar](50),
	[Bereitstellungsdatum] [varchar](50)
)

GO

