﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Paket_2._3.Models
{
    public class Mitarbeiter : Benutzer
    {
        public string RaumNummer { get; set; }
        public string Aufgabenbereich { get; set; }

        public Mitarbeiter()
        {
            Typ = "Mitarbeiter";

        }

        public Mitarbeiter(Benutzer user)
        {
            Load(user);
            
            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
            string query = " SELECT Mitarbeiter.*" +
                           " FROM [Mitarbeiter]" +
                           " WHERE [Mitarbeiter].[Nickname] = @Nickname";
            
            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Nickname", Nickname);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    RaumNummer = reader["RaumNr"].ToString();
                    Aufgabenbereich = reader["Aufgabenbereich"].ToString();
                }
                else throw new Exception("Der Mitarbeiter existiert nicht.");
            }
            conn.Close();
        }


        public static Mitarbeiter GetMitarbeiterByUser(string nickname)
        {
            return new Mitarbeiter(Benutzer.GetBenutzerByNickname(nickname));
        }
    }
}