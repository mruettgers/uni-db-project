﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Paket_2._3.Models
{
    public class BreadCrumb
    {

        public string Action { get; set; }
        public string Controller { get; set; }
        public string Module { get; set; }
        public string Title { get; set; }
        public Boolean Active { get; set; }

        public BreadCrumb()
        {
            Module = "";
            Active = false;
            Controller = "";
            Title = "";
            Active = false;
        }

        public Boolean IsActive()
        {
            return Active;
        }        

        private static List<BreadCrumb> _breadcrumbs = null;

        public static List<BreadCrumb> GetBreadCrumbs()
        {
            if (_breadcrumbs == null)
            {
                _breadcrumbs = new List<BreadCrumb>();
            }
            return _breadcrumbs;
        }

        public static void ClearBreadCrumbs()
        {
            GetBreadCrumbs().Clear();
        }

        public static void AddBreadCrumb(BreadCrumb bc)
        {
            GetBreadCrumbs().Add(bc);
        }

    }
}