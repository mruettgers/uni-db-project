﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace Paket_2._3.Models
{
    public class FlashMessage
    {

        public enum MessageType
        {
            Success, Error
        }

        private static List<FlashMessage> _flashMessages = null;
 
        public MessageType Type { get; set; }
        public string Message { get; set; }

        public static List<FlashMessage> GetFlashMessages()
        {
            if (_flashMessages == null)
            {
                _flashMessages = new List<FlashMessage>();
            }
            return _flashMessages;
        }

        public static void ClearFlashMessages()
        {
            GetFlashMessages().Clear();
        }

        public static void AddFlashMessage(string message, MessageType type)
        {
            var tmp = new FlashMessage();
            tmp.Message = message;
            tmp.Type = type;
            GetFlashMessages().Add(tmp);
        }


    }
}