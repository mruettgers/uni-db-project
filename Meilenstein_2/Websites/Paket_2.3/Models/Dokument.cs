﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Paket_2._3.Models
{
    public class Dokument
    {

        public int ID { get; set; }
        public string Kategorie { get; set; }
        public string Datei { get; set; }
        public string Titel { get; set; }
        public DateTime BereitstellungsDatum { get; set; }

        public static List<Dokument> GetDokumenteByUser(string nickname)
        {
            return GetDokumenteByUser(nickname, "Titel");
        }


        public static List<Dokument> GetDokumenteByUser(string nickname, string sorting)
        {
            List<Dokument> result = new List<Dokument>();

            const string connStr = @"Data Source=localhost\SQLExpress;Initial Catalog=Praktikum;Integrated Security=True";
            var conn = new SqlConnection(connStr);
            conn.Open();
            string query = " SELECT d.ID, d.Datei, d.Kategorie, d.Titel,d.Bereitstellungsdatum " +
                           " FROM Dokumente AS d " +
                           " WHERE d.Benutzer = @Nickname" +
                           " ORDER BY " + sorting;
            
            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Nickname", nickname);
            using(SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read()) {
                    var doc = new Dokument();
                    doc.ID = (int) reader["ID"];
                    doc.Kategorie = reader["Kategorie"].ToString();
                    doc.Datei = reader["Datei"].ToString();
                    doc.Titel = reader["Titel"].ToString();
                    doc.BereitstellungsDatum = (DateTime)reader["Bereitstellungsdatum"];
                    result.Add(doc);
                }
            }
            conn.Close();
            return result;
        }
    }
}