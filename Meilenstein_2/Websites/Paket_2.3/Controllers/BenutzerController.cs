﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Paket_2._3.Models;

namespace Paket_2._3.Controllers
{
    public class BenutzerController : Controller
    {

        // GET: /Benutzer/
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        // GET: /Benutzer/List
        public ActionResult List()
        {
            return View(Benutzer.GetBenutzer());
        }


        // GET: /Benutzer/Summary
        public ActionResult Summary(string user)
        {
            return View(Benutzer.GetBenutzerByNickname(user));
        }

        // GET: /Benutzer/Add
        public ActionResult Add()
        {
            return View(new Student());
        }

        // POST: /Benutzer/Add
        [HttpPost]
        public ActionResult Add(FormCollection fc)
        {
           
            var student = new Student();
            student.Nickname = fc["Nickname"];
            student.Vorname = fc["Vorname"];
            student.Nachname = fc["Nachname"];
            student.Email = fc["Email"];
            student.Passwort = fc["Passwort"];

            int tmp = 0;
            Int32.TryParse(fc["MatrikelNummer"], out tmp);
            student.MatrikelNummer = tmp;

            DateTime tmpdate;
            DateTime.TryParse(fc["EinschreibeDatum"], out tmpdate);
            student.EinschreibeDatum = tmpdate;

            ViewBag.ErrorMessages = null;
            var errorMessages = new List<string>();
            var valid = student.Validate(ref errorMessages);
            if (fc["Passwort"].Length == 0 || fc["Passwort"] != fc["PasswortWdh"])
            {
                valid = false;
                errorMessages.Add("Das Passwort ist ungültig oder es stimmt nicht mit der Wiederholung überein.");
            }

            if (valid)
            {
                student.Save();
                FlashMessage.AddFlashMessage( "Der Benutzer wurde erfolgreich hinzugefügt.", FlashMessage.MessageType.Success);
                return RedirectToAction("List");
            }
            else
                ViewBag.ErrorMessages = errorMessages;

            return View(student);
        }




    }
}
