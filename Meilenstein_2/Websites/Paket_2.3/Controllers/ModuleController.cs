﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Paket_2._3.Models;

namespace Paket_2._3.Controllers
{
    public class ModuleController : Controller
    {
        
        // GET: /Module/
        public ActionResult Index()
        {
            BreadCrumb.ClearBreadCrumbs();
            return RedirectToAction("List");
        }

        // GET: /Module/List
        public ActionResult List()
        {
            return View(Modul.GetModule());
        }

    }
}
