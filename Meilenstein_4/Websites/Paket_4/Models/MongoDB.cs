﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB;
using MongoDB.Driver;
using MongoDB.Bson;

namespace DBForum.Models
{
    public class MongoDB
    {

        const string C_MONGO_CONSTRING = @"mongodb://10.11.11.15:27017";
        

        protected static MongoClient _connection = null;
        public static MongoClient connection {
            get { return _connection ?? (_connection = new MongoClient(C_MONGO_CONSTRING)); }
        }


        public static IEnumerable<IGrouping<string, Bogenantwort> > getResult(int modulID) {
            //Load questions
            var db = connection.GetServer().GetDatabase("dbforum");
            var collF = db.GetCollection("evaluation.forms");
            var query = new QueryDocument("ModuleID", modulID);
            var el = collF.FindOne(query);
            if (el != null)
            {
                var colA = db.GetCollection("evaluation.answers");
                query = new QueryDocument("Form", el.GetValue("_id").AsObjectId);

                var answers = colA.Find(query);
                if (answers.Any())
                {

                    return from ag in
                               (
                                   from answer in answers
                                   join question in el.GetValue("Questions").AsBsonArray
                                     on answer.GetValue("QuestionID").AsString equals question.AsBsonDocument.GetValue("ID").AsString
                                   select new Bogenantwort
                                 {
                                     Antwort = answer.GetValue("Answer").AsString,
                                     Bogenfrage = Bogenfrage.FromBsonDocument(question.AsBsonDocument)
                                 }
                                   )
                           group ag by ag.Bogenfrage.Frage;

                }

            }
            return null;
        }


        public static void saveEvaluation(int modulID, IEnumerable<Bogenantwort> antworten)
        {
            
            var db = connection.GetServer().GetDatabase("dbforum");
            var collF = db.GetCollection("evaluation.forms");
            var collA = db.GetCollection("evaluation.answers");
            var query = new QueryDocument("ModuleID", modulID);

            var el = collF.FindOne(query);
            if (el != null)
            {
                
                var answers = new BsonArray();
                foreach (var a in antworten)
                {
                    var doc = a.ToBsonDocument();
                    doc.Add("Form", el.GetValue("_id").AsObjectId);
                    collA.Insert(doc);
                }

            }

        }


        public static IEnumerable<Bogenantwort> getEmptyBogen(int modulID)
        {

            var db = connection.GetServer().GetDatabase("dbforum");
            var coll = db.GetCollection("evaluation.forms");
            var query = new QueryDocument("ModuleID", modulID);
            var el = coll.FindOne(query);

            IEnumerable<Bogenantwort> result = null;
            if (el != null)
            {
                result = from q in el.GetValue("Questions").AsBsonArray
                         select new Bogenantwort
                         {
                             Bogenfrage = Bogenfrage.FromBsonDocument(q.AsBsonDocument),
                             Antwort = ""
                         };
            }
            return result;
        }


        public static void insertBogen(int modulID, IEnumerable<Bogenfrage> fragen)
        {
            var db = connection.GetServer().GetDatabase("dbforum");
            var collF = db.GetCollection("evaluation.forms");

            var query = new QueryDocument("ModuleID", modulID);
            
            var el = collF.FindOne(query);

            if (el != null) {
                //Remove old answers
                var collA = db.GetCollection("evaluation.answers");
                query = new QueryDocument("Form", el.GetValue("_id").AsObjectId);
                collA.Remove(query);
            }

            var questions = new BsonArray();
            foreach (var q in fragen) {
                questions.Add(q.ToBsonDocument());
            }

            if (el != null) {
               //Update
                el.Set("Questions", questions);
                el.Set("Modified", DateTime.Now);
            }
            else
            {
                el = new BsonDocument {
                    {"ModuleID", modulID},
                    {"Questions", questions},
                    {"Created", DateTime.Now},
                    {"Modified", null}
                };
            }
            collF.Save(el);
         }
    }
}