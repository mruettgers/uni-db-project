﻿using MongoDB;
using MongoDB.Bson;

namespace DBForum.Models
{
    public class Bogenantwort
    {
        // Jede antwort enthält noch einmal die Frage selbst
        public Bogenfrage Bogenfrage { get; set; }

        // Die Benutzerantwort
        public string Antwort { get; set; }

        public BsonDocument ToBsonDocument()
        {
            return new BsonDocument {
                {  "QuestionID", Bogenfrage.ID },
                {  "Answer", Antwort }
            };
        }
    
    }


}