﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace DBForum.Models
{
    public class Forum
    {

        public int ID { get; set; }
        public string Bezeichnung { get; set; }
        public int OberforumID { get; set; }
 

        public static List<Forum> GetForen()
        {
            return GetForen("Bezeichnung");
        }

        public static List<Forum> GetForen(string sorting)
        {
            var result = new List<Forum>();
            var conn = new SqlConnection(MvcApplication.ConnectionString);
            conn.Open();
            string query =
                "SELECT ID, ISNULL(OberforumID,0) AS OberforumID, Bezeichnung FROM Foren";
            
            if (sorting != null)
            {
                query += " ORDER BY " + sorting;
            }

            var cmd = new SqlCommand(query, conn);
            
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    var forum = new Forum();
                    forum.ID = (int) reader["ID"];
                    forum.OberforumID = (int) reader["OberforumID"];
                    forum.Bezeichnung = reader["Bezeichnung"].ToString();
                 
                    result.Add(forum);
                }
            }
            conn.Close();
            return result;
        }

    }
}