﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Web;

namespace DBForum.Models.Linq
{
    public class NeueDiskussion
    {
        public Foren AktuellesForum { get; set; }
        public Stack<Foren> Path { get; set; }
        public String Title { get; set; }
        public String Message { get; set; }
        public String User { get; set; }

        public NeueDiskussion()
        {
            Init();
        }

        public NeueDiskussion(int forumId)
        {
            Init();
            SetForumId(forumId);
        }


        protected void Init()
        {
            Path = null;
            AktuellesForum = null;
            Title = "";
            Message = "";
            User = "";
        }

        public void SetForumId(int forumId)
        {
            var fp =  new ForenPfad(forumId);
            Path = fp.Path;
            AktuellesForum = fp.AktuellesForum;
        }

        public bool Validate(ref List<String> errorMessages)
        {
            var valid = true;

            if (AktuellesForum == null)
            {
                errorMessages.Add("Es wurde kein Forum angegeben.");
                valid = false;
            }
            else
            {
                var dc = new DataClasses1DataContext();
                var tmp = from forum in dc.Foren
                    where forum.ID == AktuellesForum.ID
                    select forum;
                if (!tmp.Any())
                {
                    errorMessages.Add("Das angegebene Forum konnte nicht gefunden werden.");
                    valid = false;
                }
            }

            if (DBForum.Models.Benutzer.GetBenutzerByNickname(User) == null)
            {
                errorMessages.Add("Der angegebene Benutzer existiert nicht.");
                valid = false;
            }

            if (String.IsNullOrEmpty(Title))
            {
                errorMessages.Add("Der Titel darf nicht leer sein.");
                valid = false;
            }
            if (String.IsNullOrEmpty(Message))
            {
                errorMessages.Add("Zu der Diskussion muss eine Mitteilung angegeben werden.");
                valid = false;
            }
            return valid;
        }

        public void Save()
        {
            var dc = new DataClasses1DataContext();

            var d = new Diskussionen()
            {
                ForumID = AktuellesForum.ID,
                Titel = Title
            };
            dc.Diskussionen.InsertOnSubmit(d);
            dc.SubmitChanges();
            var b = new Beiträge()
            {
                Mitteilung = Message,
                DiskussionsID = d.ID,
                Benutzer = User,
                Änderungsdatum = DateTime.Now
            };
            dc.Beiträge.InsertOnSubmit(b);
            dc.SubmitChanges();
        }
    }
}