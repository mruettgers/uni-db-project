﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBForum.Models.Linq
{
    public class ForenPfad
    {
        public Stack<Foren> Path { get; set; }
        public Foren AktuellesForum { get; set; }

        public ForenPfad(int forumId)
        {
            var dc = new DataClasses1DataContext();
            Path = null;
            AktuellesForum = null;
            if (forumId != 0)
            {
                var tmp = (from forum in dc.Foren
                    where forum.ID == forumId
                    select forum);

                if (tmp.Any())
                {
                    AktuellesForum = tmp.First();
                }
            }
            BuildPath();
        }

        public ForenPfad(Foren forum)
        {
            Path = null;
            AktuellesForum = forum;
            BuildPath();
        }

        protected void BuildPath()
        {
            if (AktuellesForum != null)
            {
                var dc = new DataClasses1DataContext();

                //Get path
                Path = new Stack<Foren>();
                Path.Push(AktuellesForum);
                IEnumerable<Foren> tmp;
                do
                {
                    tmp = (from forum in dc.Foren
                        where forum.ID == Path.First().OberforumID
                        select forum);
                    if (tmp.Any())
                    {
                        Path.Push(tmp.First());
                    }

                } while (tmp.Any() && Path.First().OberforumID != 0);

            }    
        }

    }
}