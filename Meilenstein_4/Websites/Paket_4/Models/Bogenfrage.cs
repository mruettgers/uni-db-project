﻿using System.Collections.Generic;
using MongoDB;
using MongoDB.Bson;

// TODO: namespace anpassen
namespace DBForum.Models
{
    public class Bogenfrage
    {
        public string ID { get; set; }
        
        // Fragetext
        public string Frage { get; set; }

        // gültige werte: text, yesno, select
        public string Typ { get; set; }

        // optional (nur bei typ = select)
        public IEnumerable<string> VordefinierteWerte { get; set; }

        // Convert BsonDocument to Bogenfrage
        public static Bogenfrage FromBsonDocument(BsonDocument doc)
        {
            var result = new Bogenfrage
            {
                VordefinierteWerte = null,
                ID = doc.GetValue("ID").AsString,
                Frage = doc.GetValue("Question").AsString,
                Typ = doc.GetValue("Type").AsString
            };
            if (result.Typ == "select")
            {
                var tmp = new List<string>();
                foreach (var el in doc.GetValue("Options").AsBsonArray)
                {
                    tmp.Add(el.AsString);
                }
                result.VordefinierteWerte = tmp;
            }
            return result;
        }

        public BsonDocument ToBsonDocument()
        {
            var result = new BsonDocument {
                {"ID", ID},
                {"Question", Frage},  
                {"Type", Typ}
            };

            if (Typ == "select")
            {
                var options = new BsonArray();
                foreach (var option in VordefinierteWerte) {
                    options.Add(option);
                }
                result.Add("Options", options);
            }
            return result;
        }

    }
}