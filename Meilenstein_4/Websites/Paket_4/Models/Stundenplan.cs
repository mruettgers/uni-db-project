﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Xml;


namespace DBForum.Models
{
    public class Stundenplan
    {
        public int FachSemester { get; set; }
        public DateTime Aktualisiert { get; set; }
        public string Studiengang { get; set; }
        public List<Tag> Tage { get; set; }
        
        public static string[] C_ZEITEN = { "08:15", "10:15", "12:15", "14:15", "15:45", "17:15" };
        public static string[] C_TAGE = { "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag" };
        
        public Stundenplan()
        {
            Tage = new List<Tag>();
        }


        public static IEnumerable<int> GetFachnummer(XElement root)
        {

            var result =
                      from block in
                          (from tag in root.Descendants("Tag")
                            select tag).Descendants("Block")

                      where block.Attribute("FachNr") != null
                      && block.Attribute("FachNr").Value.Length > 0
            
                      select int.Parse(block.Attribute("FachNr").Value);

            return result.Distinct();
        }


        public static Stundenplan LoadFromXML(string file) {
            var result = new Stundenplan();

            var finfo = new FileInfo(file);
            if (finfo.Exists) {

                
                //Load xml doc
                var xdoc = XDocument.Load(file);

                var fachnummern = GetFachnummer(xdoc.Root);
                var dc = new Linq.DataClasses1DataContext();
                var zusaetze = from m in dc.Module
                               join b in dc.Benutzer on m.Verantwortlicher equals b.Nickname into bgj
                               join f in dc.Foren on m.ForumID equals f.ID into fgj
                               where fachnummern.Contains(m.FachNummer)
                               from sb in bgj.DefaultIfEmpty()
                               from sf in fgj.DefaultIfEmpty()
                               select new
                               {
                                    modul = m,
                                    benutzer = sb,
                                    forum = sf
                               };

                
                if (xdoc.Root.Attribute("Aktualisiert") != null) 
                    result.Aktualisiert = DateTime.Parse(xdoc.Root.Attribute("Aktualisiert").Value);

                if (xdoc.Root.Attribute("FachSemester") != null) 
                    result.FachSemester = int.Parse(xdoc.Root.Attribute("FachSemester").Value);

                if (xdoc.Root.Attribute("Studiengang") != null) 
                    result.Studiengang = xdoc.Root.Attribute("Studiengang").Value;
                

                var xtage = from xtag in xdoc.Root.Descendants("Tag")
                           select xtag;
                if (xtage.Any())
                {
                    foreach (var xtag in xtage) {
                        var tag = new Tag { Name = "", Blöcke = new List<Block>() } ;

                        if (xtag.Attribute("Name") != null) {
                            tag.Name = xtag.Attribute("Name").Value;


                            var xbloecke = from xblock in xtag.Descendants("Block")
                                           join z in zusaetze on 
                                               (xblock.Attribute("FachNr") == null ? 0 : int.Parse(xblock.Attribute("FachNr").Value)) equals z.modul.FachNummer
                                                into zgj
                                           from sz in zgj.DefaultIfEmpty()
                                           orderby (string)xblock.Attribute("Zeit")
                                           select new
                                           {
                                               node = xblock,
                                               modul = (sz != null && sz.modul != null) ? sz.modul : null,
                                               forum = (sz != null && sz.forum != null) ? sz.forum : null,
                                               benutzer = (sz != null && sz.benutzer != null) ? sz.benutzer : null
                                           };

                            if (xbloecke.Any())
                            {
                                foreach (var xblock in xbloecke)
                                {

                                    if (!String.IsNullOrWhiteSpace(xblock.node.Value))
                                    {

                                        var block = new Block
                                        {
                                            Zeit = null,
                                            FachNr = 0,
                                            Typ = "",
                                            Veranstaltung = xblock.node.Value
                                        };

                                        if (xblock.node.Attribute("Zeit") != null)
                                            block.Zeit = xblock.node.Attribute("Zeit").Value;

                                        if (xblock.node.Attribute("FachNr") != null)
                                            block.FachNr = int.Parse(xblock.node.Attribute("FachNr").Value);

                                        if (xblock.node.Attribute("Typ") != null)
                                            block.Typ = xblock.node.Attribute("Typ").Value;

                                        block.Modul = xblock.modul;
                                        block.Forum = xblock.forum;
                                        block.Verantwortlicher = xblock.benutzer;


                                        tag.Blöcke.Add(block);
                                    }
                                    else tag.Blöcke.Add(null);
                                    
                                }
                            }


                        }
                        result.Tage.Add(tag);
                    }
                }
                 
               
            }
            else throw new Exception("Die angegebene XML-Datei existiert nicht.");

            return result;
        }



        public static Stundenplan GetMockupDaten()
        {
            

            var result = new Stundenplan
                             {
                                 Aktualisiert = DateTime.Now,
                                 FachSemester = 1,
                                 Studiengang = "English for runaways",
                             };

            // montag
            foreach (var t in C_TAGE)
            {
                var tag = new Tag {Name = t, Blöcke = new List<Block>()};

                foreach (var zeit in C_ZEITEN)
                {
                    tag.Blöcke.Add(new Block
                    {
                        Zeit = zeit,
                        FachNr = 973,
                        Typ = "V",
                        Veranstaltung = "The art of Brathering"
                    });
                }

                result.Tage.Add(tag);
            }

            return result;
        }
    }

    public class Tag
    {
        public string Name { get; set; }
        public List<Block> Blöcke { get; set; }

        public Tag()
        {
            Blöcke = new List<Block>();
        }   
    }

    public class Block
    {
        
        public string Zeit { get; set; }
        public string Veranstaltung { get; set; }

        public string TypReadable
        {
            get
            {
                switch (Typ)
                {
                    case "V": return "Vorlesung";
                    case "P": return "Praktikum";
                    case "Ü": return "Übung";
                } //SWITCH
                return Typ;
            }
        }
        public string Typ { get; set; }

        public int FachNr { get; set; }
        public Linq.Module Modul { get; set; }
        public Linq.Benutzer Verantwortlicher { get; set; }
        public Linq.Foren Forum { get; set; }


        /*
        public Linq.Benutzer Verantwortlicher { get; private set; }
        public Linq.Foren Forum { get; private set; }

        protected int _FachNr = 0;
        public int FachNr { 
            get {
                return _FachNr; 
            }

            set
            {
                //Fetch user and forum
                //FIXME: Add some caching
                if (value != _FachNr)
                {
                    _FachNr = value;
                    var dc = new Linq.DataClasses1DataContext();
                    var modules = from modul in dc.Module
                                  where modul.FachNummer == FachNr
                                  select modul;

                    if (modules.Any())
                    {
                        Verantwortlicher = (from user in dc.Benutzer
                                   where user.Nickname == modules.First().Verantwortlicher
                                   select user).First();
                        Forum = (from forum in dc.Foren
                                           where forum.ID == modules.First().ForumID
                                           select forum).First();
                    }
                }                
            } 
        }
        */
       

    }
}