﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace DBForum.Models
{
    public class Student : Benutzer
    {
        public int MatrikelNummer { get; set; }
        public DateTime EinschreibeDatum { get; set; }

        public Student()
        {
            Typ = "Student";
        }

        public Student(Benutzer user)
        {
            Load(user);

            var conn = new SqlConnection(MvcApplication.ConnectionString);
            conn.Open();
            string query = " SELECT Studenten.*" +
                           " FROM [Studenten]" +
                           " WHERE [Studenten].[Nickname] = @Nickname";

            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Nickname", Nickname);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    MatrikelNummer = (int) reader["Matrikel"];
                    EinschreibeDatum = (DateTime) reader["EinschreibeDatum"];
                }
                else throw new Exception("Der Student existiert nicht.");
            }
            conn.Close();
        }


        public static Boolean ExistsStudent(int martikelnr)
        {
            var conn = new SqlConnection(MvcApplication.ConnectionString);
            conn.Open();
            string query = " SELECT COUNT(*) AS AnzErgebnisse " +
                                 " FROM [Studenten]" +
                                 " WHERE 1=1 ";

            query += " AND [Studenten].[Matrikel] = @Matrikelnr";
           
            var cmd = new SqlCommand(query, conn);

            cmd.Parameters.AddWithValue("@Matrikelnr",  martikelnr);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    int tmp;
                    Int32.TryParse(reader["AnzErgebnisse"].ToString(), out tmp);
                    if (tmp == 0)
                        return false;
                }
            }
            conn.Close();
            return true;
        }


        public static Student GetStudentByUser(string nickname)
        {
            return new Student(Benutzer.GetBenutzerByNickname(nickname));
        }


        public override bool Validate(ref List<String> errorMessages)
        {
            var parentRes = base.Validate(ref errorMessages);
            var res = parentRes;
            if (MatrikelNummer == 0)
            {
                errorMessages.Add("Es wurde keine gültige Matrikel-Nummer angegeben.");
                res = false;
            }
            else if (ExistsStudent(MatrikelNummer))
            {
                errorMessages.Add("Es existiert bereits ein Student mit dieser Matrikel-Nummer.");
                res = false;
            }

            return res;
        }


        public override void Save()
        {
            base.Save();
            string query = "";
            if (SaveAction == SaveActionType.Update)
            {
                throw new Exception("Update action is not implemented yet.");
                /*
                query = "UPDATE Student SET ";
                query += " WHERE Nickname=@Nickname";
                */

            }
            else if (SaveAction == SaveActionType.Create)
            {
                query = "INSERT INTO Studenten (Nickname,Matrikel,EinschreibeDatum) VALUES " +
                        " (@Nickname,@MatrikelNummer,@EinschreibeDatum)";

            }

            var conn = new SqlConnection(MvcApplication.ConnectionString);
            conn.Open();

            var cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Nickname", Nickname);
            cmd.Parameters.AddWithValue("@MatrikelNummer", MatrikelNummer);
            cmd.Parameters.AddWithValue("@EinschreibeDatum", EinschreibeDatum.ToString("yyyy-M-dd"));
            cmd.ExecuteNonQuery();
            conn.Close();
        }



    }
}