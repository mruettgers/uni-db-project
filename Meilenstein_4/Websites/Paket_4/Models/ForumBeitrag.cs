﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Data.SqlClient;

namespace DBForum.Models
{
    public class ForumBeitrag
    {
        public Forum Forum { get; set; }
        public List<Beitrag> Beitraege { get; set; }

        public static List<ForumBeitrag> GetForenBeitraegeByUser(string nickname)
        {
            var result = new List<ForumBeitrag>();
            foreach (Forum f in Forum.GetForen())
            {
                var fb = new ForumBeitrag();
                fb.Forum = f;
                fb.Beitraege = Beitrag.GetBeitraegeByForumAndUser(f.ID, nickname);
                if (fb.Beitraege != null && fb.Beitraege.Count > 0)
                {
                    result.Add(fb);
                }
            }
            return result;
        } 

   }

}