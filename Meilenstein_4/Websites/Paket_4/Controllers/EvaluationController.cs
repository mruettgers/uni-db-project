﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBForum.Models;
using DBForum.Models.Linq;

// TODO: namespace anpassen
namespace DBForum.Controllers
{
    public partial class EvaluationController : Controller
    {
        #region Datenkontext

        
        // Datenverbindung zur SQL DB
        public DataClasses1DataContext _data;
        public DataClasses1DataContext Data
        {
            get { return _data ?? (_data = new DataClasses1DataContext(MvcApplication.ConnectionString)); }
        }

        // Diese Funktion gehört üblicherweise in das Model !
        private Module GetModul(int id)
        {
            var modul = (from m in Data.Module
                         where m.ID == id
                         select m).FirstOrDefault();

            if (modul == null)
                throw new ArgumentException("Unbekanntes Modul (ID = " + id + ") ungültig!");

            return modul;
        }

        #endregion

        #region FERTIGE ACTIONS

        // Modul- und Aktionen übersicht
        public ActionResult Index()
        {
            var model = from m in Data.Module select m;

            return View(model);
        }
        
        // Leitet an weitere actions, je nach Benutzerauswahl
        public ActionResult Dispatcher(int modulID, string auswahl)
        {
            if (auswahl == "erstellen")
                return RedirectToAction("CreateBogen", new { modulID });

            if (auswahl == "evaluieren")
                return RedirectToAction("Evaluate", new { modulID });

            if (auswahl == "anzeigen")
                return RedirectToAction("ShowEvaluations", new { modulID });

            throw new ArgumentException("Unerwarteter Fehler: Auswahl ungültig!", "auswahl");
        }

        // Zur Erstellung eines Evaluationsbogens
        public ActionResult CreateBogen(int modulID)
        {
            // Modul nur zur Anzeigen/Referenzierung, daher ViewBag
            ViewBag.Modul = GetModul(modulID);

            return View();
        }
        
        #endregion

        #region TODO ACTIONS

        // Evaluations-formular darstellen
        public ActionResult Evaluate(int modulID)
        {
            var modul = GetModul(modulID);
            ViewBag.Modul = modul;
            var model = DBForum.Models.MongoDB.getEmptyBogen(modulID);
            if (model == null)
            {
                FlashMessage.AddFlashMessage("Zu diesem Modul existiert noch kein Fragebogen!", FlashMessage.MessageType.Error);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        // Evaluation speichern
        [HttpPost]
        public ActionResult Evaluate(int modulID, IEnumerable<Bogenantwort> antworten)
        {
            var modul = GetModul(modulID);
            DBForum.Models.MongoDB.saveEvaluation(modulID, antworten);
            FlashMessage.AddFlashMessage("Ihre Antworten für die Evaluierung des Moduls \"" + modul.Bezeichnung + "\" wurde erfolgreich gespeichert.", FlashMessage.MessageType.Success);
            return RedirectToAction("Index");
            
//            ViewBag.Modul = modul;
//            return View();
        }

        // Fragebogen zu Modul speichern
        [HttpPost]
        public ActionResult CreateBogen(int modulID, IEnumerable<Bogenfrage> fragen)
        {
            var modul = GetModul(modulID);
            DBForum.Models.MongoDB.insertBogen(modulID, fragen);
            FlashMessage.AddFlashMessage("Der Fragebogen für das Modul \""+modul.Bezeichnung+"\" wurde erfolgreich gespeichert.", FlashMessage.MessageType.Success);
            return RedirectToAction("Index");
            //ViewBag.Modul = modul;
            //return View();
        }

        // TODO: Evaluationsergebnisse für ein Modul zeigen
        public ActionResult ShowEvaluations(int modulID)
        {
            var modul = GetModul(modulID);
            ViewBag.ModulName = modul.Bezeichnung;
            var model = DBForum.Models.MongoDB.getResult(modulID);
            if (model == null)
            {
                FlashMessage.AddFlashMessage("Zu diesem Modul existiert noch kein Fragebogen!", FlashMessage.MessageType.Error);
                return RedirectToAction("Index");
            }
            else {
                return View(model);
            }
        }

        #endregion
    }
}
