﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using DBForum.Models;
using DBForum.Models.PraktikumTableAdapters;

namespace DBForum.Controllers
{
    public class DokumenteController : Controller
    {

        // GET: /Dokumente/
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }


        // GET: /Benutzer/List
        public ActionResult List()
        {
            var adapter = new DokumenteTableAdapter();
            var model = adapter.GetDataOfAllKategorien();
            return View(model);
        }

        // GET: /Benutzer/ListContent (AJAX REQUEST)
        public ActionResult ListContent(string category)
        {
            var adapter = new DokumenteTableAdapter();
           
            Praktikum.DokumenteDataTable model;
            if (string.IsNullOrEmpty(category))
                model = adapter.GetDataOfAllKategorien();
            else 
                model = adapter.GetDataByKategorie(category);

            //Ugly workaround for making the ajax request slow
            // for testing the loading animation
            Thread.Sleep(500);

            return PartialView("Partials/ListContent", model);
        }

    }
}
