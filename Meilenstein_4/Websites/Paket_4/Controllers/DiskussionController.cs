﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBForum.Models;
using DBForum.Models.Linq;

namespace DBForum.Controllers
{
    public class DiskussionController : Controller
    {

        protected Diskussionen Diskussion { get; set; }


        //
        // GET: /Diskussion/
        public ActionResult Index(int id = 0)
        {
            var dc = new DataClasses1DataContext();
            IEnumerable<Beiträge> model = null;
            InitDiskussionAndForumPath(id);

            if (Diskussion != null)
            {
                dc.DiskussionGesichtet(id);
                model = from b in dc.Beiträge
                    where b.DiskussionsID == Diskussion.ID
                    orderby b.Änderungsdatum ascending
                    select b;
            }
            return View(model);
        }



        // GET: /Foren/AddDiscussion
        [HttpPost]
        public ActionResult AddMessage(int id, Beiträge b)
        {
            var errorMessages = new List<string>();
            InitDiskussionAndForumPath(id);
            b.DiskussionsID = id;

            if (String.IsNullOrWhiteSpace(b.Mitteilung))
            {
                errorMessages.Add("Sie müssen eine Nachricht angeben.");
            }

            if (String.IsNullOrWhiteSpace(b.Benutzer) || DBForum.Models.Benutzer.GetBenutzerByNickname(b.Benutzer) == null)
            {
                errorMessages.Add("Der angegebene Benutzer existiert nicht.");
            }

            if (errorMessages.Count == 0)
            {
                var dc = new DataClasses1DataContext();

                b.Änderungsdatum = DateTime.Now;
                dc.Beiträge.InsertOnSubmit(b);
                dc.SubmitChanges();

                FlashMessage.AddFlashMessage("Der Beitrag wurde erfolgreich erstellt.", FlashMessage.MessageType.Success);
                return RedirectToAction("Index", new { id = b.DiskussionsID });
            }
            else
            {
                ViewBag.ErrorMessages = errorMessages;
            }
            return View(b);
        }

        public ActionResult AddMessage(int id)
        {
            InitDiskussionAndForumPath(id);
            return View(new Beiträge
            {
                DiskussionsID = id
            });
        }


        protected void InitDiskussionAndForumPath(int id)
        {
            var dc = new DataClasses1DataContext();
            
            //Load current discussion
            ViewBag.Diskussion = null;
            ViewBag.ForenPath = null;
           
            var tmp = from d in dc.Diskussionen
                where d.ID == id
                select d;
            if (tmp.Any())
            {
                Diskussion = tmp.First();
                ViewBag.Diskussion = Diskussion;
                var fp = new ForenPfad(Diskussion.ForumID);
                ViewBag.ForenPath = fp.Path;
            }
        }


    }
}
