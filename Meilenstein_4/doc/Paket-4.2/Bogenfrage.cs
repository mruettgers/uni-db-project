﻿using System.Collections.Generic;

// TODO: namespace anpassen
namespace Forum.Models
{
    public class Bogenfrage
    {
        public string ID { get; set; }
        
        // Fragetext
        public string Frage { get; set; }

        // gültige werte: text, yesno, select
        public string Typ { get; set; }

        // optional (nur bei typ = select)
        public IEnumerable<string> VordefinierteWerte { get; set; }
    }
}