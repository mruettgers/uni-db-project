﻿// TODO: namespace anpassen
namespace Forum.Models
{
    public class Bogenantwort
    {
        // Jede antwort enthält noch einmal die Frage selbst
        public Bogenfrage Bogenfrage { get; set; }

        // Die Benutzerantwort
        public string Antwort { get; set; }
    }
}