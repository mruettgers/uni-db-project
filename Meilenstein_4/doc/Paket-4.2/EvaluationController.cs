﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forum.Models;
using Forum.Models.Linq;

// TODO: namespace anpassen
namespace Forum.Controllers
{
    public partial class EvaluationController : Controller
    {
        #region Datenkontext

        // TODO: Ihre SQL Verbindungszeichenfolge
        const string CONSTRING = @"Data Source=....";

        // Datenverbindung zur SQL DB
        public ForumDataDataContext _data;
        public ForumDataDataContext Data
        {
            get { return _data ?? (_data = new ForumDataDataContext(CONSTRING)); }
        }

        // Diese Funktion gehört üblicherweise in das Model !
        private Module GetModul(int id)
        {
            var modul = (from m in Data.Modules
                         where m.ID == id
                         select m).FirstOrDefault();

            if (modul == null)
                throw new ArgumentException("Unbekanntes Modul (ID = " + id + ") ungültig!");

            return modul;
        }

        #endregion

        #region FERTIGE ACTIONS

        // Modul- und Aktionen übersicht
        public ActionResult Index()
        {
            var model = from m in Data.Modules select m;

            return View(model);
        }
        
        // Leitet an weitere actions, je nach Benutzerauswahl
        public ActionResult Dispatcher(int modulID, string auswahl)
        {
            if (auswahl == "erstellen")
                return RedirectToAction("CreateBogen", new { modulID });

            if (auswahl == "evaluieren")
                return RedirectToAction("Evaluate", new { modulID });

            if (auswahl == "anzeigen")
                return RedirectToAction("ShowEvaluations", new { modulID });

            throw new ArgumentException("Unerwarteter Fehler: Auswahl ungültig!", "auswahl");
        }

        // Zur Erstellung eines Evaluationsbogens
        public ActionResult CreateBogen(int modulID)
        {
            // Modul nur zur Anzeigen/Referenzierung, daher ViewBag
            ViewBag.Modul = GetModul(modulID);

            return View();
        }
        
        #endregion

        #region TODO ACTIONS

        // TODO: Evaluations-formular darstellen
        public ActionResult Evaluate(int modulID)
        {
            return View();
        }

        // TODO: Evaluation speichern
        [HttpPost]
        public ActionResult Evaluate(int modulID, IEnumerable<Bogenantwort> antworten)
        {
            return View();
        }

        // TODO: Fragebogen zu Modul speichern
        [HttpPost]
        public ActionResult CreateBogen(int modulID, IEnumerable<Bogenfrage> fragen)
        {
            return View();
        }

        // TODO: Evaluationsergebnisse für ein Modul zeigen
        public ActionResult ShowEvaluations(int modulID)
        {
            return View();
        }

        #endregion
    }
}
